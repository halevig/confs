# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias ls='ls --color'
alias ll='ls -l --color'
alias l='ls'

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# keys
stty -ixon
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'

# Exports
export TERM='screen-256color'
export EDITOR='vim'
export LESS_TERMCAP_mb=$(printf "\e[1;31m")
export LESS_TERMCAP_md=$(printf "\e[1;31m")
export LESS_TERMCAP_me=$(printf "\e[0m")
export LESS_TERMCAP_se=$(printf "\e[0m")
export LESS_TERMCAP_so=$(printf "\e[1;44;33m")
export LESS_TERMCAP_ue=$(printf "\e[0m")
export LESS_TERMCAP_us=$(printf "\e[1;32m")
export PROMPT_COMMAND='printf "\033]2;${HOSTNAME}\033\\"'
export PAGER=less
export GIT_PAGER='less -R'
export PS1='\[\e[38;5;22m\][\[\e[38;5;34m\]\u\[\e[38;5;`if [ "$?" == "0" ]; then echo -n '"46"'; else echo -n '"196"'; fi;`m\]@\[\e[38;5;118m\]\h\[\e[38;5;190m\] \W\[\e[38;5;226m\]\[\e[38;5;226m\]]\[\e[38;5;220m\]\$\[\e[0m\] '
if [[ $HOSTNAME == "centosprod" ]]; then
  export PS1='\[\e[38;5;88m\][\[\e[38;5;160m\]\u\[\e[38;5;`if [ "$?" == "0" ]; then echo -n '"130"'; else echo -n '"160"'; fi;`m\]@\[\e[38;5;202m\]\h\[\e[38;5;214m\] \W\[\e[38;5;226m\]\[\e[38;5;190m\]]\[\e[38;5;190m\]\$\[\e[0m\] '
fi

function vimrc {
    $EDITOR ~/.vimrc && . ~/.vimrc
}

function bashrc {
    $EDITOR ~/.bashrc && . ~/.bashrc
}

function rmpyc {
    echo "REMOVING:"
    find . -name "*.pyc" | tee /dev/stderr | xargs rm
}

function rmcache {
    echo "REMOVING:"
    find . -name "__pycache__" | tee /dev/stderr | xargs rm -r
}

function auto_tmux {
  local name=${1:-$(whoami)}
  if [ -z "$TMUX" ]; then
    if tmux has-session -t $name; then
      tmux -u attach-session -t $name
    else
      tmux -u -2 new-session -s $name
    fi
  else
    tmux detach
  fi
}


if [ -z "$TMUX" ]; then
  auto_tmux
fi
